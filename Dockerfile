# https://mxnet.apache.org/get_started/validate_mxnet#docker-with-cpu
# nvidia-docker run -it mxnet/python:gpu bash
# docker run -it mxnet/python bash

FROM kundajelab/cuda-anaconda-base:latest
RUN apt-get update && apt-get install -y --no-install-recommends \
    python3.5 \
    python3-pip \
    && \
apt-get clean && \
rm -rf /var/lib/apt/lists/*
RUN pip3 install numpy matplotlib
RUN pip3 install numba
